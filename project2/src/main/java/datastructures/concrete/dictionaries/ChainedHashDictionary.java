package datastructures.concrete.dictionaries;

import datastructures.concrete.KVPair;
import datastructures.interfaces.IDictionary;
import misc.exceptions.NoSuchKeyException;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * See the spec and IDictionary for more details on what each method should do
 */
public class ChainedHashDictionary<K, V> implements IDictionary<K, V> {
    // You may not change or rename this field: we will be inspecting
    // it using our private tests.
    private IDictionary<K, V>[] chains;
    private int bucketSize;
    private int totalSize;

    // You're encouraged to add extra fields (and helper methods) though!

    public ChainedHashDictionary() {
    		this.bucketSize = 10;
    		this.totalSize = 0;
    		this.chains = this.makeArrayOfChains(bucketSize);
    		this.initiateChains(this.chains, bucketSize);
    }

    /**
     * This method will return a new, empty array of the given size
     * that can contain IDictionary<K, V> objects.
     *
     * Note that each element in the array will initially be null.
     */
    @SuppressWarnings("unchecked")
    private IDictionary<K, V>[] makeArrayOfChains(int size) {
        // Note: You do not need to modify this method.
        // See ArrayDictionary's makeArrayOfPairs(...) method for
        // more background on why we need this method.
        return (IDictionary<K, V>[]) new IDictionary[size];
    }

    private IDictionary<K, V> findBucket(K key) {
    		int index = getIndex(key);
    		return this.chains[index];
    }
    
    private void bucketCheck(IDictionary<K, V> bucket, K key) {
    		if  (!bucket.containsKey(key)) {
    			throw new NoSuchKeyException();
    		}
    }
    
    private void initiateChains(IDictionary<K, V>[] chain, int size) {
    		for (int i = 0; i < size; i++) {
    			chain[i] = new ArrayDictionary<K, V>();
		}
    }
    
    @Override
    public V get(K key) {
    		IDictionary<K, V> bucket = findBucket(key);
    		bucketCheck(bucket, key);
    		return bucket.get(key);
    }

    @Override
    public void put(K key, V value) {
    		if (((this.totalSize + 1.0) / (1.0 * this.bucketSize)) > 1.0) {
    			IDictionary<K, V>[]  oldPair = this.chains;
    			this.bucketSize *= 2;
    			this.totalSize = 0;
    			this.chains = makeArrayOfChains(this.bucketSize);
    			this.initiateChains(this.chains, this.bucketSize);
    			for (int i = 0; i < this.bucketSize / 2; i++) {
    				for (KVPair<K, V> pair : oldPair[i]) {
    					this.ordinaryPut(pair.getKey(), pair.getValue());
    				}
    			}
    		}
    		ordinaryPut(key, value);
    }
    
    private int getIndex(K key) {
    		int index = 0;
    		if (key != null) {
			index = key.hashCode() % this.bucketSize;
		}
		if (index < 0) {
			index = 0 - index;
		}
		return index;
    }
    
    private void ordinaryPut(K key, V value) {
    		int index = getIndex(key);
    		this.totalSize = this.totalSize - chains[index].size();
    		this.chains[index].put(key, value);
    		this.totalSize = this.totalSize + chains[index].size();
    }
    
    @Override
    public V remove(K key) {
		IDictionary<K, V> bucket = findBucket(key);
		bucketCheck(bucket, key);
		this.totalSize--;
		return bucket.remove(key);
    }

    @Override
    public boolean containsKey(K key) {
        return findBucket(key).containsKey(key);
    }

    @Override
    public int size() {
    		return this.totalSize;
    }

    @Override
    public Iterator<KVPair<K, V>> iterator() {
        // Note: you do not need to change this method
        return new ChainedIterator<>(this.chains, this.totalSize);
    }

    /**
     * Hints:
     *
     * 1. You should add extra fields to keep track of your iteration
     *    state. You can add as many fields as you want. If it helps,
     *    our reference implementation uses three (including the one we
     *    gave you).
     *
<<<<<<< HEAD
     * 2. Think about what exactly your *invariants* are. Once you've
     *    decided, write them down in a comment somewhere to help you
     *    remember.
     *
     * 3. Before you try and write code, try designing an algorithm
     *    using pencil and paper and run through a few examples by hand.
     *
     *    We STRONGLY recommend you spend some time doing this before
     *    coding. Getting the invariants correct can be tricky, and
     *    running through your proposed algorithm using pencil and
     *    paper is a good way of helping you iron them out.
=======
     * 2. Before you try and write code, try designing an algorithm
     *    using pencil and paper and run through a few examples by hand.
     *
     * 3. Think about what exactly your *invariants* are. An *invariant*
     *    is something that must *always* be true once the constructor is
     *    done setting up the class AND must *always* be true both before and
     *    after you call any method in your class.
     *
     *    Once you've decided, write them down in a comment somewhere to
     *    help you remember.
     *
     *    You may also find it useful to write a helper method that checks
     *    your invariants and throws an exception if they're violated.
     *    You can then call this helper method at the start and end of each
     *    method if you're running into issues while debugging.
     *
     *    (Be sure to delete this method once your iterator is fully working.)
>>>>>>> e9154deef026c8fbe3e89b0544561f18888df89f
     *
     * Implementation restrictions:
     *
     * 1. You **MAY NOT** create any new data structures. Iterators
     *    are meant to be lightweight and so should not be copying
     *    the data contained in your dictionary to some other data
     *    structure.
     *
     * 2. You **MAY** call the `.iterator()` method on each IDictionary
     *    instance inside your 'chains' array, however.
     */
    private static class ChainedIterator<K, V> implements Iterator<KVPair<K, V>> {
        private IDictionary<K, V>[] chain;
        private int currentItr;
        private int totalSize;
        private int currentPair;
        private Iterator<KVPair<K, V>> iterator;
        
        public ChainedIterator(IDictionary<K, V>[] chains, int totalSize) {
            this.chain = chains;
            this.currentItr = 0;
            this.currentPair = 0;
            this.totalSize = totalSize;
            this.iterator = this.chain[0].iterator();
        }

        @Override
        public boolean hasNext() {
            return this.currentPair < this.totalSize;
        }

        @Override
        public KVPair<K, V> next() {
        		if (!this.hasNext()) {
    				throw new NoSuchElementException();
        		}
        		while (!iterator.hasNext()) {
        			this.currentItr++;
        			this.iterator = this.chain[this.currentItr].iterator();
        		}
        		this.currentPair++;
        		return iterator.next();
        }
    }
}