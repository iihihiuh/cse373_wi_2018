package datastructures.sorting;

import misc.BaseTest;
import datastructures.concrete.DoubleLinkedList;
import datastructures.interfaces.IList;
import misc.Searcher;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * See spec for details on what kinds of tests this class should include.
 */
public class TestTopKSortFunctionality extends BaseTest {
   
    @Test(timeout = SECOND)
    public void testSimpleUsage() {
        IList<Integer> list = new DoubleLinkedList<>();
        for (int i = 0; i < 20; i++) {
            list.add(i);
        }
        IList<Integer> top = Searcher.topKSort(5, list);
        assertEquals(5, top.size());
        for (int i = 0; i < top.size(); i++) {
            assertEquals(15 + i, top.get(i));
        }
    }

    @Test(timeout = SECOND)
    public void testOneElement() {
        IList<Integer> list = new DoubleLinkedList<>();
        list.add(17);

        IList<Integer> top = Searcher.topKSort(5, list);
        assertEquals(1, top.size());
        assertEquals(17, top.get(0));
    }

    @Test(timeout = SECOND)
    public void testException() {
        IList<Integer> list = new DoubleLinkedList<>();
        list.add(17);

        try {
            Searcher.topKSort(-1, list);
        } catch (IllegalArgumentException ex) {
            // This is ok: do nothing
        }
    }

    @Test(timeout = SECOND)
    public void testRandomFive() {
        IList<Integer> list = new DoubleLinkedList<>();
        list.add(16);
        list.add(9);
        list.add(20);
        list.add(37);
        list.add(28);
        list.add(45);

        IList<Integer> top = Searcher.topKSort(3, list);
        assertEquals(3, top.size());
        assertTrue(top.contains(28));
        assertTrue(top.contains(37));
        assertTrue(top.contains(45));
    }

    @Test(timeout = SECOND)
    public void testNull() {
        IList<Integer> list = new DoubleLinkedList<>();
        IList<Integer> top = Searcher.topKSort(3, list);
        assertEquals(0, top.size());
    }

    @Test(timeout = SECOND)
    public void testNullList() {
        try {
            Searcher.topKSort(3, null);
        } catch (IllegalArgumentException ex) {
            // doing nothing is ok
        }
    }
}
