package datastructures.concrete;

import datastructures.interfaces.IPriorityQueue;
import misc.exceptions.EmptyContainerException;

/**
 * See IPriorityQueue for details on what each method must do.
 */
public class ArrayHeap<T extends Comparable<T>> implements IPriorityQueue<T> {
    // See spec: you must implement a implement a 4-heap.
    private static final int NUM_CHILDREN = 4;

    // You MUST use this field to store the contents of your heap.
    // You may NOT rename this field: we will be inspecting it within
    // our private tests.
    private T[] heap;
    private int totalSize;
    // Feel free to add more fields and constants.

    public ArrayHeap() {
    		this.heap = makeArrayOfT(21);
    		this.totalSize = 0;
    }

    /**
     * This method will return a new, empty array of the given size
     * that can contain elements of type T.
     *
     * Note that each element in the array will initially be null.
     */
    @SuppressWarnings("unchecked")
    private T[] makeArrayOfT(int size) {
        // This helper method is basically the same one we gave you
        // in ArrayDictionary and ChainedHashDictionary.
        //
        // As before, you do not need to understand how this method
        // works, and should not modify it in any way.
        return (T[]) (new Comparable[size]);
    }

    @Override
    public T removeMin() {
    		if (this.totalSize == 0) {
    			throw new EmptyContainerException();
    		}
    		T item = this.heap[0];
    		this.totalSize--;
    		this.heap[0] = this.heap[totalSize];
    		precDown(0);
    		return item;
    }

    @Override
    public T peekMin() {
    		if (this.totalSize == 0) {
    			throw new EmptyContainerException();
    		}
    		return this.heap[0];
    }

    @Override
    public void insert(T item) {
    		if (item == null) {
    			throw new IllegalArgumentException();
    		}
    		if (this.totalSize >= this.heap.length) {
    			T[] newHeap = makeArrayOfT(this.heap.length * 2);
    			for (int i = 0; i < this.totalSize; i++) {
    				newHeap[i] = this.heap[i];
    			}
    			this.heap = newHeap;
    		}
    		this.heap[this.totalSize] = item;
    		this.totalSize++;
    		precUp(totalSize - 1);
    }

    @Override
    public int size() {
    		return this.totalSize;
    }
    
    private void precDown(int index) {
    		if (getChild(index, 1) < this.totalSize) {
    			int smallIndex = smallestChild(index);
    			T parent = this.heap[index];
    			if (parent.compareTo(this.heap[smallIndex]) > 0) {
    				this.heap[index] = heap[smallIndex];
    				this.heap[smallIndex] = parent;
    				precDown(smallIndex);
    			}
    		}
    }
    
    private void precUp(int index) {
    		if (index != 0) {
    			int pIndex = this.getParent(index);
    			T parent = this.heap[pIndex];
    			if (parent.compareTo(heap[index]) > 0) {
    				this.heap[pIndex] = this.heap[index];
    				this.heap[index] = parent;
    				precUp(pIndex);
    			}
    		}
    }
    
    private int getChild(int index, int which) {
    		return NUM_CHILDREN * index + which;
    }
    
    private int smallestChild(int index) {
    		int child = NUM_CHILDREN * index + 1; 
    		int small = child;
    		T min = this.heap[child];
    		for (int i = 1; i <= 3 && child + i < this.totalSize; i++) {
    			if (min.compareTo(heap[child + i]) > 0) {
    				small = child + i;
    				min = heap[child + i];
    			}
    		}
    		return small;
    }
    
    private int getParent(int index) {
    		return (index - 1) / NUM_CHILDREN;
    }
}
