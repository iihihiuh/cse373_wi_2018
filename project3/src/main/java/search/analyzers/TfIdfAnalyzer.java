package search.analyzers;

import datastructures.concrete.KVPair;
import datastructures.concrete.dictionaries.ChainedHashDictionary;
import datastructures.interfaces.IDictionary;
import datastructures.interfaces.IList;
import datastructures.interfaces.ISet;
import search.models.Webpage;

import java.net.URI;

/**
 * This class is responsible for computing how "relevant" any given document is
 * to a given search query.
 *
 * See the spec for more details.
 */
public class TfIdfAnalyzer {
    // This field must contain the IDF score for every single word in all
    // the documents.
    private IDictionary<String, Double> idfScores;

    // This field must contain the TF-IDF vector for each webpage you were given
    // in the constructor.
    //
    // We will use each webpage's page URI as a unique key.
    private IDictionary<URI, IDictionary<String, Double>> documentTfIdfVectors;
    
    private IDictionary<URI, Double> norm;  
    
    // Feel free to add extra fields and helper methods.

    public TfIdfAnalyzer(ISet<Webpage> webpages) {
        // Implementation note: We have commented these method calls out so your
        // search engine doesn't immediately crash when you try running it for the
        // first time.
        //
        // You should uncomment these lines when you're ready to begin working
        // on this class.

        this.idfScores = this.computeIdfScores(webpages);
        this.documentTfIdfVectors = this.computeAllDocumentTfIdfVectors(webpages);
        this.norm = new ChainedHashDictionary<>();
        for (KVPair<URI, IDictionary<String, Double>> pair : this.documentTfIdfVectors) {
        		norm.put(pair.getKey(), norm(pair.getValue()));
        }
    }

    private Double norm(IDictionary<String, Double> value) {
    		double result = 0.0;
    		for (KVPair<String, Double> pair : value) {
    			result += pair.getValue() * pair.getValue();
    		}
    		return Math.sqrt(result);
	}

    // Note: this method, strictly speaking, doesn't need to exist. However,
    // we've included it so we can add some unit tests to help verify that your
    // constructor correctly initializes your fields.
    public IDictionary<URI, IDictionary<String, Double>> getDocumentTfIdfVectors() {
        return this.documentTfIdfVectors;
    }

    // Note: these private methods are suggestions or hints on how to structure your
    // code. However, since they're private, you're not obligated to implement exactly
    // these methods: feel free to change or modify these methods however you want. The
    // important thing is that your 'computeRelevance' method ultimately returns the
    // correct answer in an efficient manner.

    /**
     * Return a dictionary mapping every single unique word found
     * in every single document to their IDF score.
     */
    private IDictionary<String, Double> computeIdfScores(ISet<Webpage> pages) {
        IDictionary<String, Double> dicts = new ChainedHashDictionary<>();
        // word count pagenum
        IDictionary<String, KVPair<Integer, Integer>> count = new ChainedHashDictionary<>();
        int pageNum = 0;
        // page count
        for (Webpage page : pages) {
        		for (String word : page.getWords()) {
        			if (count.containsKey(word)) {
        				int countNum = count.get(word).getKey(); // num of counts
        				int wordPage = count.get(word).getValue(); // page num
        				if (wordPage != pageNum) {
        					count.put(word, new KVPair<>(countNum + 1, pageNum));
        				}
        			} else {
        				count.put(word, new KVPair<>(1, pageNum));
        			}
        		}
        		pageNum++;
        }
        
        // idf computation
        for (KVPair<String, KVPair<Integer, Integer>> pair : count) {
        		double idf = 0.0;
        		int numCount = pair.getValue().getKey();
        		if (numCount != 0) {
        			idf = Math.log((double) ((double) pages.size() / (double) numCount));
        		}
        		dicts.put(pair.getKey(), idf);
        }
        return dicts;
    }

    /**
     * Returns a dictionary mapping every unique word found in the given list
     * to their term frequency (TF) score.
     *
     * The input list represents the words contained within a single document.
     */
    private IDictionary<String, Double> computeTfScores(IList<String> words) {
        IDictionary<String, Double> dicts = new ChainedHashDictionary<>();
        IDictionary<String, Integer> count = new ChainedHashDictionary<>();
        // count num of appearance
        for (String word : words) {
        		// first seen
        		if (!count.containsKey(word)) {
        			count.put(word, 1);
        		} else {
        			// increment
        			count.put(word, count.get(word) + 1);
        		}
        }
        // compute tf
        for (KVPair<String, Integer> pair : count) {
        		double appears = (double) pair.getValue();
        		dicts.put(pair.getKey(), (double) (appears / (double) words.size()));
        }
        return dicts;
    }

    /**
     * See spec for more details on what this method should do.
     */
    private IDictionary<URI, IDictionary<String, Double>> computeAllDocumentTfIdfVectors(ISet<Webpage> pages) {
    		IDictionary<URI, IDictionary<String, Double>> dicts = new ChainedHashDictionary<>();
    		for (Webpage page : pages) {
    			IDictionary<String, Double> tfIdf = new ChainedHashDictionary<>();
    			IDictionary<String, Double> tf = this.computeTfScores(page.getWords());
    			for (KVPair<String, Double> pair : tf) {
    				tfIdf.put(pair.getKey(), pair.getValue() * this.idfScores.get(pair.getKey()));
    			}
    			dicts.put(page.getUri(), tfIdf);
    		}
    		return dicts;
    }

    /**
     * Returns the cosine similarity between the TF-IDF vector for the given query and the
     * URI's document.
     *
     * Precondition: the given uri must have been one of the uris within the list of
     *               webpages given to the constructor.
     */
    public Double computeRelevance(IList<String> query, URI pageUri) {
    		IDictionary<String, Double> documentScores = this.documentTfIdfVectors.get(pageUri);
    		IDictionary<String, Double> queryScores = this.computeTFIdfVectors(query);
    		double numerator = 0.0;
    		double demA = 0.0;
    		double demB = this.norm.get(pageUri);
    		for (KVPair<String, Double> pair : queryScores) {
    			double biggerScore = 0.0;
    			if (documentScores.containsKey(pair.getKey())) {
    				biggerScore = documentScores.get(pair.getKey());
    			}
    			double smallerScore = pair.getValue();
    			numerator += smallerScore * biggerScore;
    			demA += smallerScore * smallerScore;
    		}
    		double dem = Math.sqrt(demA) * demB;
    		if (dem != 0) {
    			return numerator / dem;
    		}
    		return 0.0;
    }
    
    private IDictionary<String, Double> computeTFIdfVectors(IList<String> query) {
        IDictionary<String, Double> dicts = new ChainedHashDictionary<>();
        IDictionary<String, Integer> count = new ChainedHashDictionary<>();
        // count num of appearance
        for (String word : query) {
        		// first seen
        		if (!count.containsKey(word)) {
        			count.put(word, 1);
        		} else {
        			// increment
        			count.put(word, count.get(word) + 1);
        		}
        }
        
        // compute tfidr
        for (KVPair<String, Integer> pair : count) {
        		double appears = (double) pair.getValue();
        		double idscore = 0;
        		if (this.idfScores.containsKey(pair.getKey())) {
        			idscore = this.idfScores.get(pair.getKey());
        		}
        		dicts.put(pair.getKey(), ((double) (appears / (double) query.size())) * idscore);
        	}
        return dicts;
    }
}
