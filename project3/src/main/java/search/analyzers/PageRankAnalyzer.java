package search.analyzers;

import datastructures.concrete.ChainedHashSet;
import datastructures.concrete.KVPair;
import datastructures.concrete.dictionaries.ChainedHashDictionary;
import datastructures.interfaces.IDictionary;
import datastructures.interfaces.IList;
import datastructures.interfaces.ISet;
import search.models.Webpage;

import java.net.URI;

/**
 * This class is responsible for computing the 'page rank' of all available webpages.
 * If a webpage has many different links to it, it should have a higher page rank.
 * See the spec for more details.
 */
public class PageRankAnalyzer {
    private IDictionary<URI, Double> pageRanks;

    /**
     * Computes a graph representing the internet and computes the page rank of all
     * available webpages.
     *
     * @param webpages  A set of all webpages we have parsed.
     * @param decay     Represents the "decay" factor when computing page rank (see spec).
     * @param epsilon   When the difference in page ranks is less then or equal to this number,
     *                  stop iterating.
     * @param limit     The maximum number of iterations we spend computing page rank. This value
     *                  is meant as a safety valve to prevent us from infinite looping in case our
     *                  page rank never converges.
     */
    public PageRankAnalyzer(ISet<Webpage> webpages, double decay, double epsilon, int limit) {
        // Implementation note: We have commented these method calls out so your
        // search engine doesn't immediately crash when you try running it for the
        // first time.
        //
        // You should uncomment these lines when you're ready to begin working
        // on this class.

        // Step 1: Make a graph representing the 'internet'
        IDictionary<URI, ISet<URI>> graph = this.makeGraph(webpages);

        // Step 2: Use this graph to compute the page rank for each webpage
        this.pageRanks = this.makePageRanks(graph, decay, limit, epsilon);

        // Note: we don't store the graph as a field: once we've computed the
        // page ranks, we no longer need it!
    }

    /**
     * This method converts a set of webpages into an unweighted, directed graph,
     * in adjacency list form.
     *
     * You may assume that each webpage can be uniquely identified by its URI.
     *
     * Note that a webpage may contain links to other webpages that are *not*
     * included within set of webpages you were given. You should omit these
     * links from your graph: we want the final graph we build to be
     * entirely "self-contained".
     */
    private IDictionary<URI, ISet<URI>> makeGraph(ISet<Webpage> webpages) {
        ISet<URI> selfContain = new ChainedHashSet<>();
        for (Webpage everyWebPage: webpages) {
            selfContain.add(everyWebPage.getUri());
        }
        IDictionary<URI, ISet<URI>> output = new ChainedHashDictionary<>();
        for (Webpage webPage: webpages) {
            URI currentURI = webPage.getUri();
            IList<URI> links = webPage.getLinks();
            ISet<URI> internalSet = new ChainedHashSet<>();
            for (URI link: links) {
                if ((!link.equals(currentURI)) && (selfContain.contains(link))) {
                    internalSet.add(link);
                }
            }
            output.put(currentURI, internalSet);
        }
        return output;
    }

    /**
     * Computes the page ranks for all webpages in the graph.
     *
     * Precondition: assumes 'this.graphs' has previously been initialized.
     *
     * @param decay     Represents the "decay" factor when computing page rank (see spec).
     * @param epsilon   When the difference in page ranks is less then or equal to this number,
     *                  stop iterating.
     * @param limit     The maximum number of iterations we spend computing page rank. This value
     *                  is meant as a safety valve to prevent us from infinite looping in case our
     *                  page rank never converges.
     */
    private IDictionary<URI, Double> makePageRanks(IDictionary<URI, ISet<URI>> graph,
                                                   double decay,
                                                   int limit,
                                                   double epsilon) {
    		// Step 1: Initialization
    		IDictionary<URI, Double> rankDictionary = new ChainedHashDictionary<>();
    		double initScore = 1.0 / (double) graph.size();
    		for (KVPair<URI, ISet<URI>> pair : graph) {
    			rankDictionary.put(pair.getKey(), initScore);
    		}
    
        double surfer = (double) (1.0 - decay) / (double) graph.size();
    		for (int i = 0; i < limit; i++) {
    			// Step2: update
    			IDictionary<URI, Double> oldRank = rankDictionary;
    			rankDictionary = new ChainedHashDictionary<>();
			for (KVPair<URI, Double> pair : oldRank) {
				rankDictionary.put(pair.getKey(), surfer);
			}
    			for (KVPair<URI, Double> pair : oldRank) {
    				ISet<URI> outLinks = graph.get(pair.getKey());
    				if (outLinks.isEmpty()) {
    					for (KVPair<URI, Double> newRankpair : oldRank) {
    						URI currentURI = newRankpair.getKey();
    						double old = rankDictionary.get(newRankpair.getKey());
    						rankDictionary.put(currentURI, old + (decay * pair.getValue() 
    								/ (double) graph.size()));
    					}
    				} else {
    					for (URI currentURI : outLinks) {
    						double old = rankDictionary.get(currentURI);
    						rankDictionary.put(currentURI, old + decay 
    							* oldRank.get(pair.getKey()) / (double) outLinks.size());
    					}
    				}
    			}
    			// Step3: converge
    			int notConvergence = 0;
                for (KVPair<URI, Double> rank: oldRank) {
                    if (Math.abs(oldRank.get(rank.getKey()) - 
                                 rankDictionary.get(rank.getKey())) >= epsilon) {
                        notConvergence++;
                    }
                }
                if (notConvergence == 0) {
                    return rankDictionary;
                }
    		}
        return rankDictionary;
    }

    /**
     * Returns the page rank of the given URI.
     *
     * Precondition: the given uri must have been one of the uris within the list of
     *               webpages given to the constructor.
     */
    public double computePageRank(URI pageUri) {
        // Implementation note: this method should be very simple: just one line!
        return pageRanks.get(pageUri);
    }
}
