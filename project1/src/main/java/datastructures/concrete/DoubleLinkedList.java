// Yongqin Wang, Tianhao Zhang
package datastructures.concrete;

import datastructures.interfaces.IList;
import misc.exceptions.EmptyContainerException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Note: For more info on the expected behavior of your methods, see
 * the source code for IList.
 */
public class DoubleLinkedList<T> implements IList<T> {
    // You may not rename these fields or change their types.
    // We will be inspecting these in our private tests.
    // You also may not add any additional fields.
    private Node<T> front;
    private Node<T> back;
    private int size;

    public DoubleLinkedList() {
        this.front = null;
        this.back = null;
        this.size = 0;
    }

    @Override
    public void add(T item) {
    		Node<T> newNode = new Node<T>(back, item, null);
    		if (this.size == 0) {
    			this.front = newNode;
    		} else {
    			this.back.next = newNode;
    		}
    		this.back = newNode;
    		this.size++;
    }

    @Override
    public T remove() {
    		if (this.size == 0) {
    			throw new EmptyContainerException();
    		}
    		Node<T> savedLast = this.back;
    		if (this.size == 1) {
    			this.front = null;
    			this.back = this.front;
    		} else {
    			this.back.prev.next = null;
    			this.back = this.back.prev;
    			savedLast.prev = null;
    		}
    		this.size--;
    		return savedLast.data;
    }

    @Override
    public T get(int index) {
    		return getNode(index).data;
    }

    @Override
    public void set(int index, T item) {
    		Node<T> removed = getNode(index);
    		Node<T> newNode = new Node<T>(removed.prev, item, removed.next);
    		if (removed.prev != null) {
    			removed.prev.next = newNode; 
    		} else {
    			this.front = newNode;
    		}
    		if (removed.next != null) {
    			removed.next.prev = newNode;
    		} else {
    			this.back = newNode;
    		}
    		removed.next = null;
    		removed.prev = null;
    }
    
    private Node<T> getNode(int index) {
    		indexCheck(index, false);	
    		Node<T> newNode = this.front;
    		if (index < size / 2) {
			for (int i = 0; i < index; i++) {
				newNode = newNode.next;
			}	
    		} else {
    			newNode = this.back;
    			for (int i = size - 1;  i > index; i--) {
    				newNode = newNode.prev;
    			}
    		}
		return newNode;	
    }
    
    private void indexCheck(int index, boolean insert) {
    		if (index < 0 || (insert & index > this.size) || (!insert & index >= this.size)) {
    			throw  new IndexOutOfBoundsException();
    		}
    }

    @Override
    public void insert(int index, T item) {
    		indexCheck(index, true);
    		Node<T> newNode = new Node<T>(item);
    		if (index == size) {
    			add(item);
    		} else if (index == 0){
    			newNode.next = this.front;
    			this.front.prev = newNode;
    			this.front = newNode;
    			this.size++;
    		} else {
    			Node<T> nextNode = getNode(index);
    			newNode.next = nextNode;
    			newNode.prev = nextNode.prev;
    			nextNode.prev.next = newNode;
    			nextNode.prev = newNode;
    		    this.size++;
    		}
    		
    }

    @Override
    public T delete(int index) {
    		indexCheck(index, false);
    		Node<T> removed = getNode(index);
    		if (index == 0) {
    			if (size == 1) {
    				this.front = null;
    				this.back = null;
    			} else {
    				front = front.next;
    				front.prev = null;
    			}
    			this.size--;
    		} else if (index == size - 1) {
    			this.remove();
    		} else {
    			removed.prev.next = removed.next;
    			removed.next.prev = removed.prev;
    			this.size--;
    		}
    		removed.next = null;
    		removed.prev = null;
    		return removed.data;
    }

    @Override
    public int indexOf(T item) {
    		Node<T> searchNode = front;
    		int index = 0;
    		while (searchNode != null) {
    			if ((searchNode.data == item) 
    					|| (searchNode.data != null && searchNode.data.equals(item))) {
    				return index;
    			}
     	    searchNode = searchNode.next;
   			index++;
    		}
    		return -1;
    }

    @Override
    public int size() {
    		return this.size;
    }

    @Override
    public boolean contains(T other) {
    		return indexOf(other) != -1;
    }

    @Override
    public Iterator<T> iterator() {
        // Note: we have provided a part of the implementation of
        // an iterator for you. You should complete the methods stubs
        // in the DoubleLinkedListIterator inner class at the bottom
        // of this file. You do not need to change this method.
        return new DoubleLinkedListIterator<>(this.front);
    }

    private static class Node<E> {
        // You may not change the fields in this node or add any new fields.
        public final E data;
        public Node<E> prev;
        public Node<E> next;

        public Node(Node<E> prev, E data, Node<E> next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }

        public Node(E data) {
            this(null, data, null);
        }

        // Feel free to add additional constructors or methods to this class.
    }

    private static class DoubleLinkedListIterator<T> implements Iterator<T> {
        // You should not need to change this field, or add any new fields.
        private Node<T> current;

        public DoubleLinkedListIterator(Node<T> current) {
            // You do not need to make any changes to this constructor.
            this.current = current;
        }

        /**
         * Returns 'true' if the iterator still has elements to look at;
         * returns 'false' otherwise.
         */
        public boolean hasNext() {
        		return current != null;
        }

        /**
         * Returns the next item in the iteration and internally updates the
         * iterator to advance one element forward.
         *
         * @throws NoSuchElementException if we have reached the end of the iteration and
         *         there are no more elements to look at.
         */
        public T next() {
        		if (!this.hasNext()) {
        			throw new NoSuchElementException();
        		}
        		Node<T> returned =  current;
        		this.current = this.current.next;
        		return returned.data;
        }
    }
}
