// Yongqin Wang, Tianhao Zhang
package datastructures.concrete.dictionaries;

import datastructures.interfaces.IDictionary;
import misc.exceptions.NoSuchKeyException;

/**
 * See IDictionary for more details on what this class should do
 */
public class ArrayDictionary<K, V> implements IDictionary<K, V> {
    // You may not change or rename this field: we will be inspecting
    // it using our private tests.
    private Pair<K, V>[] pairs;

    // You're encouraged to add extra fields (and helper methods) though!
    private int size;
    
    public ArrayDictionary() {
        pairs = makeArrayOfPairs(4);
    }

    /**
     * This method will return a new, empty array of the given size
     * that can contain Pair<K, V> objects.
     *
     * Note that each element in the array will initially be null.
     */
    @SuppressWarnings("unchecked")
    private Pair<K, V>[] makeArrayOfPairs(int arraySize) {
        // It turns out that creating arrays of generic objects in Java
        // is complicated due to something known as 'type erasure'.
        //
        // We've given you this helper method to help simplify this part of
        // your assignment. Use this helper method as appropriate when
        // implementing the rest of this class.
        //
        // You are not required to understand how this method works, what
        // type erasure is, or how arrays and generics interact. Do not
        // modify this method in any way.
        return (Pair<K, V>[]) (new Pair[arraySize]);

    }

    @Override
    public V get(K key) {
        int keyLocation = traverseArray(key);
        if (keyLocation >= 0) {
            return pairs[keyLocation].value;
        } else {
            throw new NoSuchKeyException();
        }
    }
    
    private int traverseArray(K key) {
        for (int i = 0; i < size; i++) {
            if ((pairs[i].key == null && key == null) ||
               (pairs[i].key.equals(key))) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void put(K key, V value) {
        int keyLocation = traverseArray(key);
        if (keyLocation >= 0) {
            pairs[keyLocation].value = value;
        } else {
            if ((size) >= pairs.length) {
                Pair<K, V>[] temp = makeArrayOfPairs(pairs.length * 2);
                for (int i = 0; i < pairs.length; i++) {
                    temp[i] = pairs[i];
                }
                pairs = temp;
            }
            Pair<K, V> newPair = new Pair<K, V>(key, value);
            pairs[size] = newPair;
            size++;
        }
    }

    @Override
    public V remove(K key) {
        int keyLocation = traverseArray(key);
        if (keyLocation < 0) {
            throw new NoSuchKeyException();
        } else if (keyLocation == (size - 1)) {
            size--;
            return pairs[size].value;
        } else {
            V tempValue = pairs[keyLocation].value;
            for (int i = keyLocation + 1; i < size; i++) {
                pairs[i - 1] = pairs[i];
            }
            size--;
            return tempValue;
        }  
    }

    @Override
    public boolean containsKey(K key) {
        return (traverseArray(key) >= 0);
    }

    @Override
    public int size() {
        return this.size;
    }

    private static class Pair<K, V> {
        public K key;
        public V value;

        // You may add constructors and methods to this class as necessary.
        public Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public String toString() {
            return this.key + "=" + this.value;
        }
    }
}
