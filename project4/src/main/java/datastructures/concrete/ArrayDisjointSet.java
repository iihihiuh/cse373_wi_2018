package datastructures.concrete;

import datastructures.interfaces.IDisjointSet;
import datastructures.concrete.dictionaries.ChainedHashDictionary;
import datastructures.interfaces.IDictionary;

/**
 * See IDisjointSet for more details.
 */
public class ArrayDisjointSet<T> implements IDisjointSet<T> {
    // Note: do NOT rename or delete this field. We will be inspecting it
    // directly within our private tests.
    private int[] pointers;
    private IDictionary<T, Integer> list;
    private int size;
    // However, feel free to add more methods and private helper methods.
    // You will probably need to add one or two more fields in order to
    // successfully implement this class.

    public ArrayDisjointSet() {
    		this.pointers = makeArrayOfT(40);
    		list = new ChainedHashDictionary<>();
    		this.size = 0;
    }

    @Override
    public void makeSet(T item) {
    		if (list.containsKey(item)) {
    			throw new IllegalArgumentException();
    		}
    		// add to set
    		list.put(item, this.size);
    		// resizing
    		if (this.size == this.pointers.length - 1) {
    			int[] oldPointers = this.pointers;	// new pointers
    			this.pointers = makeArrayOfT(oldPointers.length * 2);
    			for (int i = 0; i < oldPointers.length; i++) {
    				this.pointers[i] = oldPointers[i];	// migration
    			}
    		}
    		this.pointers[this.size] = -1;	// rank of zero
    		this.size++;
    }

    @Override
    public int findSet(T item) {
    		Integer index = list.getOrDefault(item, null);
    		if (index == null) {
			throw new IllegalArgumentException();
		}
    		int currentIndex = index;
    		int rank = this.pointers[index];
    		if (rank < 0) {
    			return index;
    		}
    		while (rank >= 0) {	// finding parent
    			currentIndex = rank;
    			rank = this.pointers[currentIndex];
    		}
    		this.pointers[index] = currentIndex;
    		return currentIndex;
    }
    
    @Override
    public void union(T item1, T item2) {
    		int joined = findSet(item1);
    		int joining = findSet(item2);
    		if (joined == joining) {
    			throw new IllegalArgumentException();
    		}
    		if (this.pointers[joining] < this.pointers[joined]) {
    			int temp = joined;
    			joined = joining;
    			joining = temp;
    		}
    		if (this.pointers[joining] == this.pointers[joined]) {
    			this.pointers[joined]--;
    		}
    		this.pointers[joining] = joined;
    }
    
    private int[] makeArrayOfT(int arraySize) {
        return (int[]) (new int[arraySize]);
    }
}
