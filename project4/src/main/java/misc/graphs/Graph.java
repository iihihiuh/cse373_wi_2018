package misc.graphs;

import datastructures.concrete.ArrayDisjointSet;
import datastructures.concrete.ArrayHeap;
import datastructures.concrete.ChainedHashSet;
import datastructures.concrete.DoubleLinkedList;
import datastructures.concrete.KVPair;
import datastructures.concrete.dictionaries.ChainedHashDictionary;
import datastructures.interfaces.IDictionary;
import datastructures.interfaces.IDisjointSet;
import datastructures.interfaces.IList;
import datastructures.interfaces.IPriorityQueue;
import datastructures.interfaces.ISet;
import misc.Searcher;
import misc.exceptions.NoPathExistsException;

/**
 * Represents an undirected, weighted graph, possibly containing self-loops,
 * parallel edges, and unconnected components.
 *
 * Note: This class is not meant to be a full-featured way of representing a
 * graph. We stick with supporting just a few, core set of operations needed for
 * the remainder of the project.
 */
public class Graph<V, E extends Edge<V> & Comparable<E>> {
	// NOTE 1:
	//
	// Feel free to add as many fields, private helper methods, and private
	// inner classes as you want.
	//
	// And of course, as always, you may also use any of the data structures
	// and algorithms we've implemented so far.
	//
	// Note: If you plan on adding a new class, please be sure to make it a private
	// static inner class contained within this file. Our testing infrastructure
	// works by copying specific files from your project to ours, and if you
	// add new files, they won't be copied and your code will not compile.
	//
	//
	// NOTE 2:
	//
	// You may notice that the generic types of Graph are a little bit more
	// complicated then usual.
	//
	// This class uses two generic parameters: V and E.
	//
	// - 'V' is the type of the vertices in the graph. The vertices can be
	// any type the client wants -- there are no restrictions.
	//
	// - 'E' is the type of the edges in the graph. We've contrained Graph
	// so that E *must* always be an instance of Edge<V> AND Comparable<E>.
	//
	// What this means is that if you have an object of type E, you can use
	// any of the methods from both the Edge interface and from the Comparable
	// interface
	//
	// If you have any additional questions about generics, or run into issues while
	// working with them, please ask ASAP either on Piazza or during office hours.
	//
	// Working with generics is really not the focus of this class, so if you
	// get stuck, let us know we'll try and help you get unstuck as best as we can.
	/**
	 * Constructs a new graph based on the given vertices and edges.
	 *
	 * @throws IllegalArgumentException
	 *             if any of the edges have a negative weight
	 * @throws IllegalArgumentException
	 *             if one of the edges connects to a vertex not present in the
	 *             'vertices' list
	 */

	private IDictionary<V, ISet<E>> dicts;
	private int numOfVs;
	private int numOfEs;
	IList<V> nodes;
	IList<E> paths;

	public Graph(IList<V> vertices, IList<E> edges) {
		this.numOfVs = vertices.size();
		this.numOfEs = edges.size();
		this.dicts = new ChainedHashDictionary<>();
		this.nodes = vertices;
		this.paths = edges;
		for (E edge : edges) {
			V start = edge.getVertex1();
			V end = edge.getVertex2();
			if (!this.nodes.contains(start) || !this.nodes.contains(end) || edge.getWeight() < 0) {
				throw new IllegalArgumentException();
			}
			if (!dicts.containsKey(start)) {
				dicts.put(start, new ChainedHashSet<E>());
			}
			if (!dicts.containsKey(end)) {
				dicts.put(end, new ChainedHashSet<E>());
			}
			ISet<E> startDict = dicts.get(start);
			startDict.add(edge);
			dicts.put(start, startDict);
			ISet<E> endDict = dicts.get(end);
			endDict.add(edge);
			dicts.put(end, endDict);
		}
		for (V vertice : vertices) {
			if (!this.dicts.containsKey(vertice)) {
				dicts.put(vertice, new ChainedHashSet<E>());
			}
		}
	}

	/**
	 * Sometimes, we store vertices and edges as sets instead of lists, so we
	 * provide this extra constructor to make converting between the two more
	 * convenient.
	 */
	public Graph(ISet<V> vertices, ISet<E> edges) {
		// You do not need to modify this method.
		this(setToList(vertices), setToList(edges));
	}

	// You shouldn't need to call this helper method -- it only needs to be used
	// in the constructor above.
	private static <T> IList<T> setToList(ISet<T> set) {
		IList<T> output = new DoubleLinkedList<>();
		for (T item : set) {
			output.add(item);
		}
		return output;
	}

	/**
	 * Returns the number of vertices contained within this graph.
	 */
	public int numVertices() {
		return this.numOfVs;
	}

	/**
	 * Returns the number of edges contained within this graph.
	 */
	public int numEdges() {
		return this.numOfEs;
	}

	/**
	 * Returns the set of all edges that make up the minimum spanning tree of this
	 * graph.
	 *
	 * If there exists multiple valid MSTs, return any one of them.
	 *
	 * Precondition: the graph does not contain any unconnected components.
	 */
	public ISet<E> findMinimumSpanningTree() {
		ISet<E> mst = new ChainedHashSet<>();
		IList<E> edges = Searcher.topKSort(this.paths.size(), this.paths);
		IDisjointSet<V> unions = new ArrayDisjointSet<>();
		for (V vertice : this.nodes) {
			unions.makeSet(vertice);
		}
		for (E edge : edges) {
			if (unions.findSet(edge.getVertex1()) != unions.findSet(edge.getVertex2())) {
				unions.union(edge.getVertex1(), edge.getVertex2());
				mst.add(edge);
			}
		}
		return mst;
 	}

	/**
	 * Returns the edges that make up the shortest path from the start to the end.
	 *
	 * The first edge in the output list should be the edge leading out of the
	 * starting node; the last edge in the output list should be the edge connecting
	 * to the end node.
	 *
	 * Return an empty list if the start and end vertices are the same.
	 *
	 * @throws NoPathExistsException
	 *             if there does not exist a path from the start to the end
	 */
	public IList<E> findShortestPathBetween(V start, V end) {
		IDictionary<V, KVPair<IList<E>, Double>> costs = new ChainedHashDictionary<>();
		IPriorityQueue<NodeWithCost<V>> heap = new ArrayHeap<>();
		IList<E> result = new DoubleLinkedList<E>();
		ISet<V> visited = new ChainedHashSet<V>();
		for (V node : this.nodes) {
			KVPair<IList<E>, Double> costAndPath = 
					new KVPair<>(new DoubleLinkedList<E>(), Double.POSITIVE_INFINITY);
			costs.put(node, costAndPath);
			heap.insert(new NodeWithCost<V>(null, node, Double.POSITIVE_INFINITY));
		}
		KVPair<IList<E>, Double> costAndPath = new KVPair<>(new DoubleLinkedList<E>(), 0.0);
		costs.put(start, costAndPath);
		heap.insert(new NodeWithCost<V>(null, start, 0.0));
		V currentNode = null;
		double currentCost = Double.POSITIVE_INFINITY;
		if (heap.size() > 0) {
			NodeWithCost<V> temp = heap.removeMin();
			currentNode = temp.node;
			currentCost = temp.cost;
		}
		if (currentNode == null) {
			return result;
		}
		while (!currentNode.equals(end)) {
			for (E edge : dicts.get(currentNode)) {
				double newCost = costs.get(currentNode).getValue() + edge.getWeight();
				V vertexUpdate = edge.getOtherVertex(currentNode);
				KVPair<IList<E>, Double> curPair = costs.get(currentNode);
				if (newCost < costs.get(vertexUpdate).getValue() && 
						!visited.contains(vertexUpdate)) {
					IList<E> path = new DoubleLinkedList<E>();
					for (E edg : curPair.getKey()) {
						path.add(edg);
					}
					path.add(edge);
					costs.put(vertexUpdate, new KVPair<IList<E>, Double>(path, newCost));
					heap.insert(new NodeWithCost<V>(currentNode, vertexUpdate, newCost));
				}
			}
			NodeWithCost<V> nextEdge = heap.removeMin();
			visited.add(currentNode);
			currentNode = nextEdge.node;
			currentCost = nextEdge.cost;
			result = costs.get(currentNode).getKey();
		}
		if (currentCost == Double.POSITIVE_INFINITY) {
			throw new NoPathExistsException();
		}
		return result;
	}
	
	private class NodeWithCost<V1> implements Comparable<NodeWithCost<V1>>, Edge<V1> {
		public V1 thisNode;
		public V1 node;
		public double cost;
		
		public NodeWithCost(V1 node1, V1 nodeInput, double costInput) {
			this.thisNode = node1;
			this.node = nodeInput;
			this.cost = costInput;
		}
		
		@Override
		public int compareTo(NodeWithCost<V1> other) {
			if (this.cost > other.cost) {
				return 1;
			} else if (this.cost == other.cost){
				return 0;
			} else {
				return -1;
			}
		}

		@Override
		public V1 getVertex1() {
			return this.thisNode;
		}

		@Override
		public V1 getVertex2() {
			return node;
		}

		@Override
		public double getWeight() {
			return cost;
		}
	}
}
