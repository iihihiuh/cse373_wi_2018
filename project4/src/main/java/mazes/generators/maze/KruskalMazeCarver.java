package mazes.generators.maze;

import datastructures.interfaces.ISet;
import mazes.entities.Maze;
import mazes.entities.Room;
import mazes.entities.Wall;
import misc.graphs.Graph;

import java.util.Random;

/**
 * Carves out a maze based on Kruskal's algorithm.
 *
 * See the spec for more details.
 */
public class KruskalMazeCarver implements MazeCarver {
	@Override
	public ISet<Wall> returnWallsToRemove(Maze maze) {
		// Note: make sure that the input maze remains unmodified after this method is
		// over.
		//
		// In particular, if you call 'wall.setDistance()' at any point, make sure to
		// call 'wall.resetDistanceToOriginal()' on the same wall before returning.

		ISet<Wall> allWalls = maze.getWalls();
		Random rand = new Random();
		for (Wall eachWall : allWalls) {
			eachWall.setDistance(rand.nextDouble());
		}
		Graph<Room, Wall> runningMST = new Graph<Room, Wall>(maze.getRooms(), allWalls);
		ISet<Wall> toRemove = runningMST.findMinimumSpanningTree();
		for (Wall eachWall : allWalls) {
			eachWall.resetDistanceToOriginal();
		}
		return toRemove;
	}
}
