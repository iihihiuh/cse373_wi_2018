package datastructures.sorting;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import misc.BaseTest;
import misc.exceptions.EmptyContainerException;
import datastructures.concrete.ArrayHeap;
import datastructures.interfaces.IPriorityQueue;
import org.junit.Test;

/**
 * See spec for details on what kinds of tests this class should include.
 */
public class TestArrayHeapFunctionality extends BaseTest {
	protected <T extends Comparable<T>> IPriorityQueue<T> makeInstance() {
		return new ArrayHeap<>();
	}

	protected IPriorityQueue<String> makeBasicList() {
		IPriorityQueue<String> list = new ArrayHeap<>();

		list.insert("a");
		list.insert("b");
		list.insert("c");

		return list;
	}

	@Test(timeout = SECOND)
	public void testBasicSize() {
		IPriorityQueue<Integer> heap = this.makeInstance();
		heap.insert(3);
		assertEquals(1, heap.size());
		assertTrue(!heap.isEmpty());
	}
	

    @Test(timeout = SECOND)
    public void testInsertAndRemoveWithoutResize() {
        IPriorityQueue<Integer> list = this.makeInstance();
        for (int i = 0; i < 21; i++) {
            list.insert(i);
        }
        for (int i = 0; i < 21; i++) {
            assertEquals(i, list.removeMin());
        }
    }

    @Test(timeout = SECOND)
    public void testInsertingSame() {
        IPriorityQueue<String> list = this.makeInstance();
        list.insert("foo");
        list.insert("foo");
        assertEquals("foo", list.removeMin());
        assertEquals("foo", list.removeMin());
    }
   

	@Test(timeout = SECOND)
	public void testEmpty() {
		IPriorityQueue<Integer> list = this.makeInstance();
		try {
			list.peekMin();
			fail("exception expected");
		} catch (EmptyContainerException ex) {
			// do nothing is ok
		}

		try {
			list.removeMin();
			fail("exception expected");
		} catch (EmptyContainerException ex) {
			// do nothing is ok
		}
	}

	@Test(timeout = SECOND)
	public void testNull1() {
		IPriorityQueue<Integer> list = this.makeInstance();
		for (int i = 2; i < 10; i++) {
			list.insert(3 * i);
		}
		list.insert(4);
		list.insert(5);
		list.insert(11);
		try {
			list.insert(null);
		} catch (IllegalArgumentException ex) {
			// do nothing is ok
		}
		assertEquals(4, list.peekMin());
		list.removeMin();
		assertEquals(5, list.peekMin());
		list.removeMin();
		assertEquals(6, list.peekMin());
		list.removeMin();
		assertEquals(9, list.peekMin());
		list.removeMin();
		assertEquals(11, list.peekMin());
		list.removeMin();
		assertEquals(12, list.peekMin());
		list.removeMin();
		assertEquals(15, list.peekMin());
		list.removeMin();
		assertEquals(18, list.peekMin());
	}

	@Test(timeout = SECOND)
	public void testRemove() {
		IPriorityQueue<String> list = this.makeBasicList();
		list.removeMin();
		list.removeMin();
		list.removeMin();
		try {
			list.removeMin();
			fail("Expected EmptyContainerException");
		} catch (EmptyContainerException ex) {
			// Do nothing: this is ok
		}
	}

	@Test(timeout = SECOND)
	public void testIsEmptyAndSize() {
		IPriorityQueue<String> list = this.makeInstance();
		assertEquals(true, list.isEmpty());
		list.insert("hhhhhh");
		assertEquals(1, list.size());
		list.insert("ddd");
		assertEquals(2, list.size());
		list.insert("ff");
		assertEquals(3, list.size());
		list.insert("ffdddd");
		assertEquals(4, list.size());
		list.insert("gegeoijo");
		assertEquals(5, list.size());
		list.removeMin();
		assertEquals(4, list.size());
		list.removeMin();
		assertEquals(3, list.size());
		list.removeMin();
		assertEquals(2, list.size());
		list.removeMin();
		assertEquals(1, list.size());
		list.removeMin();
		assertEquals(true, list.isEmpty());
	}

	@Test(timeout = SECOND)
	public void testResize() {
		IPriorityQueue<Integer> list = this.makeInstance();
		for (int i = 0; i < 40; i++) {
			list.insert(i);
		}
		assertEquals(40, list.size());
		for (int i = 0; i < 40; i++) {
			assertEquals(i, list.removeMin());
		}
		assertEquals(0, list.size());
	}
}
